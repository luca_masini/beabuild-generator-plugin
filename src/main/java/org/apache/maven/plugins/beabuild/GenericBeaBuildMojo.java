package org.apache.maven.plugins.beabuild;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProjectBuilder;

import java.util.Iterator;

public class GenericBeaBuildMojo extends AbstractBeaBuildMojo {

	private final String dependenciesFolder;
	private final String beabuildFilename;

	public GenericBeaBuildMojo(MavenProjectBuilder projectBuilder, String dependenciesFolder, String beabuildFilename) {
        super(projectBuilder);
		this.dependenciesFolder = dependenciesFolder;
		this.beabuildFilename = beabuildFilename;
	}

	/**
	 * Recupera le varie dipendenze del progetto e genera il contenuto del file
	 * @return
	 * @throws MojoExecutionException
	 */
	protected String createContentFile() throws MojoExecutionException {
		StringBuilder sb = new StringBuilder();
		
		for (Iterator<Artifact>  i = results.getResolvedDependencies().iterator(); i
				.hasNext();) {
			Artifact artifact = i.next();
			String artifactFilename = null;

            if (outputAbsoluteArtifactFilename) {
                artifactFilename = calculateArtifactFileName(artifact);
			}

			if (artifact.getType().equals("jar")) {
				String row = formatPath(artifactFilename) 
						+ " = "+project.getArtifact().getArtifactId() + "-"
						+ project.getArtifact().getVersion()+ getDependenciesFolder() + artifact.getFile().getName();
				info(row);
				sb.append(row+ "\n");
				
			}
		}

		return sb.toString();
	}

	private String getDependenciesFolder() {
		return dependenciesFolder;
	}

	@Override
	public String getBeaBuildFilename() {
		return beabuildFilename;
	}
}
