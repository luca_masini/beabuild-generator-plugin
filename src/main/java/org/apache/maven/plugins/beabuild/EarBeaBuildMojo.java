package org.apache.maven.plugins.beabuild;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class EarBeaBuildMojo extends AbstractBeaBuildMojo {

	private final static String EAR_BEABUILD_FILENAME = ".beabuild.txt";
	private final static String WAR_BEABUILD_FILENAME = ".warbeabuild.txt";

	// variabili private
	private List<Dependency> dependencies;

	public EarBeaBuildMojo(MavenProjectBuilder projectBuilder) {
        super(projectBuilder);
    }

    @Override
    protected void initInternal(MavenProject project2) {
        this.dependencies = super.project.getDependencies();
    }

    @Override
    public String getBeaBuildFilename() {
        return EAR_BEABUILD_FILENAME;
    }

	/**
	 * Recupera le varie dipendenze del progetto e genera il contenuto del file
	 * 
	 * @return
	 * @throws MojoExecutionException
	 */
	@Override
	protected String createContentFile() throws MojoExecutionException {
		StringBuffer sb = new StringBuffer();

		contentEarModule(sb);
        getWarBeaBuild(sb);
		contentEarDependency(sb);

		return sb.toString();
	}

	/**
	 * Per ogni tipo dipendenza war questo plugin si aspetta di trovare un file
	 * generato dal goal generator-war-beabuild<BR>
	 * Il contenuto del file viene aggiunto al beabuild delle dipendenze
	 * dell'ear
	 * 
	 * @param sb
	 * @throws MojoExecutionException
	 */
	private void getWarBeaBuild(StringBuffer sb) throws MojoExecutionException {
		for (Dependency dep: dependencies) {
			String type = dep.getType();
			String projectDir = getProjectDir(dep.getArtifactId());
			if (type.equals("war") && projectDir!=null ) {
				String path = projectDir + "/target/" + WAR_BEABUILD_FILENAME;
                File file = new File(path);
                if (!file.exists()) {
                    String disco = path.substring(0, 1);
                    String p = path.substring(2);
                    file = new File(disco + p);
                }
                
				if (file.exists()) {
					BufferedReader input = null;
					try {
						input = new BufferedReader(new FileReader(file));
						String line = null;
						while ((line = input.readLine()) != null) {
							sb.append(line + System.getProperty("line.separator"));
						}
					} catch (IOException e) {
						throw new MojoExecutionException("Error during writing file " + outputFile.getAbsolutePath() + ".", e);
					} finally {
						try {
							input.close();
						} catch (Exception e) {
							getLog().warn("Error during file " + outputFile.getAbsolutePath() + " close().", e);
						}
					}
				} else {
					  
					getLog().warn("*********************************************************************");
					getLog().warn("Could not find the dependecies's file for the project war:" + dep.getArtifactId() + ": " + file.toURI());
					getLog().warn("The split deploy may not work correctly.");
					getLog().warn("*********************************************************************");
				}
			}
		}
	}

	private void contentEarDependency(StringBuffer sb) throws MojoExecutionException {
		for (Iterator<Artifact> i = results.getResolvedDependencies().iterator(); i.hasNext();) {
			Artifact artifact = i.next();
			String artifactFilename = null;
			if (outputAbsoluteArtifactFilename) {
				try {
					artifactFilename = artifact.getFile().getAbsoluteFile().getPath();
				} catch (NullPointerException e) {
					throw new MojoExecutionException("Error creating .beabuild file", e);
				}
			}

			if (artifact.getType().equals("jar")) {
				StringBuilder row = new StringBuilder();
				
                info("Artifact: "+artifact.toString()+", type: "+artifact.getClass().getSimpleName());
                
				// When the artifact is in the source tree we get it as ActiveProjectArtifact
				if (calculateArtifactFileName(artifact)!=null) {
					String artifactFileName = calculateArtifactFileName(artifact);
					if (artifactFileName != null) {
						row.append(artifactFileName + " = APP-INF/lib/" + artifact.getArtifactId() + "-" + artifact.getVersion());
                        if( artifact.getClassifier()!=null ) {
                            row.append("-"+artifact.getClassifier());
                        }
                        row.append(".jar");
					}
				} else {
					row.append(artifactFilename + " = APP-INF/lib/" + artifact.getFile().getName());
				}
				info(row.toString());
                row.append(System.getProperty("line.separator"));
				sb.append(row.toString());

			}
		}
	}

	private void contentEarModule(StringBuffer sb) throws MojoExecutionException {
		for (int i = 0; i < dependencies.size(); i++) {
			Dependency dep = dependencies.get(i);
			String type = dep.getType();
			String projectDir = getProjectDir(dep.getArtifactId());
            if( projectDir != null ) {
                switch (type) {
                    case "war":
                        {
                        // 1
                        String row = formatPath(projectDir) + "/src/main/webapp = " + dep.getArtifactId() + "-" + dep.getVersion() + ".war";
                        info(row);
                        sb.append(row);
                        sb.append(System.getProperty("line.separator"));

                        // 2
                        row = calculateArtifactFileNameFromProjectDir(projectDir) + " = " + dep.getArtifactId() + "-" + dep.getVersion() + ".war/WEB-INF/classes";
                        info(row);
                        sb.append(row);
                        sb.append(System.getProperty("line.separator"));
                        }
                        break;
                    case "gar":
                    case "ejb":
                    case "rar":
                        String row = calculateArtifactFileNameFromProjectDir(projectDir) + " = " + dep.getArtifactId() + "-" + dep.getVersion() + "."+type;
                        info(row);
                        sb.append(row);
                        sb.append(System.getProperty("line.separator"));
                        break;
                }
            }
		}
	}
}
